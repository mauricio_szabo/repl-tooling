(ns repl-tooling.editor-integration.interpreter.promesa
  (:refer-clojure :exclude [delay spread promise
                            await map mapcat run!
                            future let loop recur])
  (:require #_[clojure.core :as c]
            [promesa.core :as p]
            [promesa.protocols :as pt]
            [sci.core :as sci]))

;; Source adapted from here: https://github.com/borkdude/nbb/blob/47d66f61199d0e52e2524f2884b85fdf8e5fcb96/src/nbb/promesa.cljs
;; Originally under the EPL-1 License

(def p-ns (sci/create-ns 'p nil))
(def pt-ns (sci/create-ns 'promesa.protocols nil))

(defn ^:macro do!
  "Execute potentially side effectful code and return a promise resolved
  to the last expression. Always awaiting the result of each
  expression."
  [_ _ & exprs]
  `(pt/-bind nil (fn [_#]
                   ~(condp = (count exprs)
                      0 `(pt/-promise nil)
                      1 `(pt/-promise ~(first exprs))
                      (reduce (fn [acc e]
                                `(pt/-bind ~e (fn [_#] ~acc)))
                              `(pt/-promise ~(last exprs))
                              (reverse (butlast exprs)))))))

(defn ^:macro let
  "A `let` alternative that always returns promise and waits for all the
  promises on the bindings."
  [_ _ bindings & body]
  (->> (reverse (partition 2 bindings))
       (reduce (fn [acc [l r]]
                 `(pt/-bind ~r (fn [~l] ~acc)))
               `(do ~@body))))
  ; `(pt/-bind nil (fn [_#]
  ;                  ~(->> (reverse (partition 2 bindings))
  ;                        (reduce (fn [acc [l r]]
  ;                                  `(pt/-bind ~r (fn [~l] ~acc)))
  ;                                `(do ~@body))))))

; (let nil nil '[a 10 b 20] '(+ a b))

(def promesa-namespace
  {'do! (sci/copy-var do! p-ns)
   'let (sci/copy-var let p-ns)
   'then (sci/copy-var p/then p-ns)
   'catch (sci/copy-var p/catch p-ns)
   'finally (sci/copy-var p/finally p-ns)})

(def promesa-protocols-namespace
  {'-bind (sci/copy-var pt/-bind pt-ns)
   '-promise (sci/copy-var pt/-promise pt-ns)})
